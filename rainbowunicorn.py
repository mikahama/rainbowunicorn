#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from random import randint

def __myexcepthook__(exctype, value, traceback):
    print exctype
    print value


def __str_to_int_list__(string):
	ret_list = []
	for c in string:
		ret_list.append(ord(c))
	return ret_list

version = "Rainbowunicorn 1.0.3"

__abc_lower__ = "qwertyuiopasdfghjklzxcvbnm"
__abc_upper__ = __abc_lower__.upper()

registers = {}
lists = {}

def __main__():
	sys.excepthook = __myexcepthook__
	try:
		script = sys.argv[1]
		args_to_pass = " ".join(sys.argv[2:])
		lists["A"] = __str_to_int_list__(args_to_pass)
		print "Using " +script
	except:
		print version
		print "Usage rainbowunicorn script.runi"
		quit()

	def read_int():
		try:
			inp = int(raw_input(""))
		except:
			inp = 0
		return inp

	def trim_int(string):
		string = string.lstrip("0")
		return int(string)

	def stringify_list(list_of_ints):
		string = ""
		for integer in list_of_ints:
			string = string + chr(integer)
		return string


	f=open(script, "r")
	text=f.read()
	f.close()
	execute_script(text)


def execute_script(text):
	text = text.replace("\n","")
	text = text.replace(" ","")
	text = text.replace("\t","")
	n = 3
	code = [text[i:i+n] for i in range(0, len(text), n)]
	__run_code__(code)

def __run_code__(code):
	global registers
	global lists
	registers = {}
	lists = {}
	i = 0
	len_code = len(code)
	last_if = False

	while i < len_code:
		command = code[i][0]
		r1 = code[i][1]
		r2 = code[i][2]
		if command == "!":
			#Store
			if r2 == "?":
				r2 = read_int()
			registers[r1] = int(r2)
		elif command == "+":
			#Sum
			registers[r1] = registers[r1] + registers[r2]
		elif command == "-":
			#Subtract
			registers[r1] = registers[r1] - registers[r2]
		elif command == "*":
			#Multiply
			registers[r1] = registers[r1] * registers[r2]
		elif command == "/":
			#Division
			registers[r1] = registers[r1] / registers[r2]
		elif command == "$":
			#Random
			registers[r1] = randint(0,registers[r2])
		elif command == "@":
			#print char
			if r1 == "@":
				if r2 in __abc_upper__:
					#list
					string_to_print = ""
					for item in lists[r2]:
						string_to_print = string_to_print+ chr(item)
					print string_to_print
				else:
					print chr(registers[r2]),
			else:
				print chr(trim_int(r1 + r2)),
		elif command == "&":
			#Print value
			if r2 in __abc_upper__:
				print lists[r2]
			else:
				print registers[r2]
		elif command == ":":
			#Put
			registers[registers[r2]] = registers[r1]
		elif command == "?":
			#User input string
			var = raw_input()
			lists[r2] = __str_to_int_list__(var)
		elif command == "#":
			#Load
			registers[r1] = registers[registers[r2]]
		elif command == "=":
			#Equal
			if registers[r1] == registers[r2]:
				last_if = True
		elif command == "<":
			#Lower than
			if registers[r1] < registers[r2]:
				last_if = True
		elif command == ">":
			#Greater than
			if registers[r1] > registers[r2]:
				last_if = True
		elif command == "_":
			#Range
			lists[r1] = range(registers[r2])
		elif command == ".":
			#Initialize list
			lists[r2] = []
		elif command == ",":
			#Lenght of a list
			registers[r2] = len(lists[r1])
		elif command == "[":
			#Read list
			file_name = stringify_list(lists[r1])
			openf = open(file_name, "rb")
			try:
				byte_array = bytearray(openf.read())
				tmp_list = []
				for byte in byte_array:
					tmp_list.append(byte)
			except Exception, e:
				tmp_list = []
			openf.close()
			lists[r2] = tmp_list
		elif command == "]":
			#Save list
			file_name = stringify_list(lists[r1])
			output = bytearray(lists[r2])
			savef = open(file_name, "wb")
			savef.write(output)
			savef.close()
		elif command in __abc_lower__:
			#Put value to a list
			try:
				lists[command.upper()][registers[r1]] = registers[r2]
			except:
				lists[command.upper()].append(registers[r2])
		elif command in __abc_upper__:
			#Load value from a list
			registers[r2] = lists[command][registers[r1]]
		else:
			#Jump command
			if last_if:
				last_if = False
				if command == "%":
					i = registers[r2]
				else:
					i = trim_int(command + r1 +r2)
				continue
		i = i+1


if __name__ == "__main__":
    __main__()